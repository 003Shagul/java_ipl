package Projects;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class MatchesWonByTeamPerYear {
    public static void main(String[] args) {
        String matchDataPath = "src/matches.csv";
        System.out.println(matchWonByTeam(matchDataPath));


    }

    public static String matchWonByTeam(String matchDataPath){

        String EachLine = "";
        Map<String, TreeMap<String, Integer>> matchesWonByTeamPerYear = new TreeMap<>();


        try {
            BufferedReader br = new BufferedReader(new FileReader(matchDataPath));
            br.readLine();

            while ((EachLine = br.readLine()) != null) {
                String[] splittedLine = EachLine.split(",");
                String winningTeamName = splittedLine[10];
                String season = (splittedLine[1]);

                if (winningTeamName == "") {
                    winningTeamName = "No result";
                }

                if (!(matchesWonByTeamPerYear.containsKey(winningTeamName))) {
                    matchesWonByTeamPerYear.put(winningTeamName, new TreeMap<>());
                }
                if ((matchesWonByTeamPerYear.containsKey(winningTeamName))) {
                    if (!((matchesWonByTeamPerYear.get( winningTeamName )).containsKey(season))){
                        matchesWonByTeamPerYear.get(winningTeamName).put(season, 0);
                    }
                    if ((matchesWonByTeamPerYear.get( winningTeamName )).containsKey(season)){
                        int increaseCount = matchesWonByTeamPerYear.get(winningTeamName).get(season) + 1;
                        matchesWonByTeamPerYear.get(winningTeamName).put(season, increaseCount);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        return matchesWonByTeamPerYear.toString();

    }
}

