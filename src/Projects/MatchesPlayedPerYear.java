package Projects;

import java.io.*;
import java.util.*;
public class MatchesPlayedPerYear {
    public static void main(String[] args) {
        String matchDataPath = "src/matches.csv";


        System.out.println(eachSeasonCount(matchDataPath));

    }
    public static String eachSeasonCount(String matchDataPath) {

        String EachLine = "";
        Map<String, Integer> matchesPlayedPerYear = new TreeMap<>();


        try {
            BufferedReader br = new BufferedReader(new FileReader(matchDataPath));

            br.readLine();

            while ((EachLine = br.readLine()) != null) {
                String[] splittedLine = EachLine.split(",");
                String season = splittedLine[1];

                if (!(matchesPlayedPerYear.containsKey(season))) {
                    matchesPlayedPerYear.put(season, 0);
                }

                if (matchesPlayedPerYear.containsKey(season)) {
                    int count = matchesPlayedPerYear.get(season) + 1;
                    matchesPlayedPerYear.replace(season, count);
                }

            }

        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        return matchesPlayedPerYear.toString();
    }

}

