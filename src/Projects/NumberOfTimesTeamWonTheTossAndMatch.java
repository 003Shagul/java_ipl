package Projects;
import java.io.*;
import java.util.*;

public class NumberOfTimesTeamWonTheTossAndMatch {
    public static void main(String[] args) {
        String matchDataPath = "src/matches.csv";
        System.out.println(numberOfTimesTeamWonTheTossAndMatch(matchDataPath));
    }

    public static String numberOfTimesTeamWonTheTossAndMatch(String matchDataPath){


        String EachLine = "";
        Map<String, Integer> numberOfTimesTeamWonTheTossAndMatch = new TreeMap();


        try {
            BufferedReader br = new BufferedReader(new FileReader(matchDataPath));

            br.readLine();

            while ((EachLine = br.readLine()) != null) {
                String[] splittedLine = EachLine.split(",");

                String tossWonTeam = splittedLine[6];
                String matchWinner = splittedLine[10];

                if (!(numberOfTimesTeamWonTheTossAndMatch.containsKey(tossWonTeam))){
                    if (tossWonTeam.equals(matchWinner)){
                        numberOfTimesTeamWonTheTossAndMatch.put(tossWonTeam,0);
                    }
                }
                if (numberOfTimesTeamWonTheTossAndMatch.containsKey(tossWonTeam) && tossWonTeam.equals(matchWinner)) {
                    int increaseCount = numberOfTimesTeamWonTheTossAndMatch.get(tossWonTeam) + 1;
                    numberOfTimesTeamWonTheTossAndMatch.replace(tossWonTeam,increaseCount);
                }


            }

        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        return numberOfTimesTeamWonTheTossAndMatch.toString();
    }
}
