package Projects;


import java.io.*;
import java.util.*;

public class TopEconomicalBowlersIn2015 {
    public static void main(String[] args) {
        String matchDataPath = "src/matches.csv";
        String deliveryDataPath = "src/deliveries.csv";
        String year = "2015";

        System.out.println(topEconomicalBowlersIn2015(matchDataPath,deliveryDataPath,year));
    }

    public static String topEconomicalBowlersIn2015(String matchDataPath, String deliveryDataPath,String year) {

        String eachMatchLine = "";
        List allMatchIdIn2015 = new ArrayList();
        Map<String, TreeMap<String, Integer>> topEconomicalBowlersIn2015 = new TreeMap<>();

        try {
            BufferedReader allMatchData = new BufferedReader(new FileReader(matchDataPath));

            allMatchData.readLine();

            while ((eachMatchLine = allMatchData.readLine()) != null ) {
                String[] splittedData = eachMatchLine.split(",");
                String matchId = splittedData[0];
                String season = splittedData[1];

                if (season.equals("2015")){
                    allMatchIdIn2015.add(matchId);
                }

            }


        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        try {
            BufferedReader allDeliveryData = new BufferedReader(new FileReader(deliveryDataPath)) ;
            String eachDeliveryLine = "";

            allDeliveryData.readLine();

            while ((eachDeliveryLine = allDeliveryData.readLine()) != null) {
                String[] splittedDeliveryData =  eachDeliveryLine.split(",");

                String deliveryMatchId = splittedDeliveryData[0];
                String bowlerName = splittedDeliveryData[8];

                int totalRuns = Integer.parseInt(splittedDeliveryData[17]);

                if (allMatchIdIn2015.contains(deliveryMatchId)) {

                    if (!(topEconomicalBowlersIn2015.containsKey(bowlerName))) {
                        topEconomicalBowlersIn2015.put(bowlerName, new TreeMap<>());
                        topEconomicalBowlersIn2015.get(bowlerName).put("balls",0);
                        topEconomicalBowlersIn2015.get(bowlerName).put("runsConceded",0);
                    }
                    if(topEconomicalBowlersIn2015.containsKey(bowlerName)){
                        int increaseBallCount = topEconomicalBowlersIn2015.get(bowlerName).get("balls") + 1;
                        int increaseConcededRuns = topEconomicalBowlersIn2015.get(bowlerName).get("runsConceded")+totalRuns;
                        topEconomicalBowlersIn2015.get(bowlerName).replace("balls",increaseBallCount);
                        topEconomicalBowlersIn2015.get(bowlerName).replace("runsConceded",increaseConcededRuns);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        Map<String, Double> topEconomyBowlers = new HashMap<>();

        topEconomicalBowlersIn2015.forEach((key,value) -> {
            String bowlerName = key;
            int ballCount = value.get("balls");
            int totalRuns = value.get("runsConceded");
            double oversCount = ballCount/6;
            double economyRate = (totalRuns/oversCount);
            String stringEconomyRate = String.format("%.2f",economyRate);

            topEconomyBowlers.put(bowlerName, Double.parseDouble(stringEconomyRate));
        });


        Set<Map.Entry<String, Double>> entrySetOfTopEconomicalBowlers = topEconomyBowlers.entrySet();

        List<Map.Entry<String, Double>> listOfTopEconomicalBowlers = new ArrayList<>(entrySetOfTopEconomicalBowlers);

        Collections.sort(listOfTopEconomicalBowlers,(o1,o2) -> o1.getValue().compareTo(o2.getValue()));

        List top10EconomicalBowlers = listOfTopEconomicalBowlers.subList(0, 1);


        return top10EconomicalBowlers.toString();
    }
}

