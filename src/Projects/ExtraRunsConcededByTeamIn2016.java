package Projects;
import java.util.*;
import java.io.*;

public class ExtraRunsConcededByTeamIn2016 {
    public static void main(String[] args) {
        String matchDataPath = "src/matches.csv";
        String deliveryDataPath = "src/deliveries.csv";
        String year = "2016";

        System.out.println(extraRunsConcededByTeamInYear(matchDataPath,deliveryDataPath,year));
    }

    public static String extraRunsConcededByTeamInYear(String matchDataPath, String deliveryDataPath,String year) {


        String eachLine = "";
        List allMatchIdIn2016 = new ArrayList();
        Map<String, Integer> extraRunsConcededByTeamIn2016 = new TreeMap<>();

        try {
            BufferedReader allMatchData = new BufferedReader(new  FileReader(matchDataPath));

            allMatchData.readLine();

            while ((eachLine = allMatchData.readLine()) != null ) {
                String[] splittedData = eachLine.split(",");
                String matchId = splittedData[0];
                String season = splittedData[1];

                if (season.equals(year)){
                    allMatchIdIn2016.add(matchId);
                }

            }


        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        try {
            BufferedReader allDeliveryData = new BufferedReader(new FileReader(deliveryDataPath)) ;
            String eachDeliveryLine = "";

            allDeliveryData.readLine();

            while ((eachDeliveryLine = allDeliveryData.readLine()) != null) {
                String[] splittedDeliveryData =  eachDeliveryLine.split(",");

                String deliveryMatchId = splittedDeliveryData[0];
                String bowlingTeam = splittedDeliveryData[3];
                int extraRuns = Integer.parseInt(splittedDeliveryData[16]);

                if (allMatchIdIn2016.contains(deliveryMatchId)) {

                    if (!(extraRunsConcededByTeamIn2016.containsKey(bowlingTeam))) {
                        extraRunsConcededByTeamIn2016.put(bowlingTeam, 0);
                    }

                    if (extraRunsConcededByTeamIn2016.containsKey(bowlingTeam)) {
                        int addedExtraRuns = extraRunsConcededByTeamIn2016.get(bowlingTeam) + extraRuns;
                        extraRunsConcededByTeamIn2016.replace(bowlingTeam, addedExtraRuns);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            return "File Not Found";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        catch (ArrayIndexOutOfBoundsException e){
            return "Index Out Of Range";
        }
        catch (NullPointerException e) {
            return null ;
        }

        return extraRunsConcededByTeamIn2016.toString();
    }
}

